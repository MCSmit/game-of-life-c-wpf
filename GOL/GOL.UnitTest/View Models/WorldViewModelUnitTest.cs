﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GOL.UI.View_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOL.UI.View_Models.Tests
{
    [TestClass()]
    public class WorldViewModelUnitTest
    {
        #region Properties
        public static WorldViewModel Model = new WorldViewModel();
        #endregion
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            Model = new WorldViewModel();
            Model.Set(new System.Windows.Controls.DataGrid());
            //Select clear pre-set pattern
            Model.SelectedPattern = Model.Patterns.First();
        }

        [TestMethod()]
        public void StepEvolutionTest()
        {
            Model.StepEvolution();
            Assert.AreEqual(1, Model.CurrentGeneration);
            Assert.AreEqual(400, Model.CurrentDeadCellCount);
            Assert.AreEqual(0, Model.CurrentLiveCellCount);
            Assert.AreEqual(0, Model.HighestLiveCellCount);
        }

        [TestMethod()]
        public void Pattern1StepEvolutionTest()
        {
            //Small Explosion pattern Test
            Model.Reset();
            Model.SelectedPattern = Model.Patterns.First(o => o.Key == 1);
            Model.StepEvolution();
            Assert.AreEqual(1, Model.CurrentGeneration);
            Assert.AreEqual(392, Model.CurrentDeadCellCount);
            Assert.AreEqual(8, Model.CurrentLiveCellCount);
            Assert.AreEqual(8, Model.HighestLiveCellCount);
        }

        [TestMethod()]
        public void ResetTest()
        {
            Model.StepEvolution();
            Model.Reset();
            Assert.AreEqual(0, Model.CurrentGeneration);
            Assert.AreEqual(0, Model.CurrentDeadCellCount);
            Assert.AreEqual(0, Model.CurrentLiveCellCount);
            Assert.AreEqual(0, Model.HighestLiveCellCount);
        }
    }
}