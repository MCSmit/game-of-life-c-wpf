﻿using GOL.UI.View_Models;

namespace GOL.UI.Commands
{
    public class StartCommand : BaseCommand<WorldViewModel>
    {
        public StartCommand(WorldViewModel model) { Model = model; }
        public override void Execute(object parameter)
        {
            Model.StartEvolution();
        }
        public override bool CanExecute(object parameter)
        {
            return true;
        }
    }
}
