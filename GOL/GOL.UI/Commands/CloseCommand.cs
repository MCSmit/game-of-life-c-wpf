﻿using GOL.UI.View_Models;

namespace GOL.UI.Commands
{
    public class CloseCommand : BaseCommand<WorldViewModel>
    {
        public CloseCommand(WorldViewModel model) { Model = model; }
        public override void Execute(object parameter)
        {
            App.Current.Shutdown();
        }
        public override bool CanExecute(object parameter)
        {
            return true;
        }
    }
}
