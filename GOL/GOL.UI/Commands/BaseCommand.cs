﻿using System;
using System.Windows.Input;

namespace GOL.UI.Commands
{
    public abstract class BaseCommand<T> : ICommand
    {
        public event EventHandler CanExecuteChanged;
        public T Model { get; set; }
        public abstract bool CanExecute(object parameter);
        public abstract void Execute(object parameter);
    }
}
