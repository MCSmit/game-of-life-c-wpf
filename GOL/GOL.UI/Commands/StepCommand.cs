﻿using GOL.UI.View_Models;

namespace GOL.UI.Commands
{
    public class StepCommand : BaseCommand<WorldViewModel>
    {
        public StepCommand(WorldViewModel model) { Model = model; }
        public override void Execute(object parameter)
        {
            Model.StepEvolution();
        }
        public override bool CanExecute(object parameter)
        {
            return true;
        }
    }
}
