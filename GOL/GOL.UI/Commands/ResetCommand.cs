﻿using GOL.UI.View_Models;

namespace GOL.UI.Commands
{
    public class ResetCommand : BaseCommand<WorldViewModel>
    {
        public ResetCommand(WorldViewModel model) { Model = model; }
        public override void Execute(object parameter)
        {
            Model.Reset();
        }
        public override bool CanExecute(object parameter)
        {
            return true;
        }
    }
}
