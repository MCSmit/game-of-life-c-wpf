﻿using GOL.UI.View_Models;

namespace GOL.UI.Commands
{
    public class PauseCommand : BaseCommand<WorldViewModel>
    {
        public PauseCommand(WorldViewModel model) { Model = model; }
        public override void Execute(object parameter)
        {
            Model.StopEvolution();
        }
        public override bool CanExecute(object parameter)
        {
            return true;
        }
    }
}
