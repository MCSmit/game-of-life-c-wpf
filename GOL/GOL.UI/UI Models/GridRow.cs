﻿using System.Collections.Generic;

namespace GOL.UI.UI_Models
{
    public class GridRow
    {
        #region Constructor
        public GridRow()
        {
            this.Row = new List<CellModel>();
        }
        #endregion

        #region Public Properties
        public List<CellModel> Row { get; set; }
        #endregion
    }
}
