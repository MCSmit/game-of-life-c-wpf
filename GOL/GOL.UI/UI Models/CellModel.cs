﻿namespace GOL.UI.UI_Models
{
    public class CellModel: BaseModel
    {
        #region Private Fields
        private bool _State;
        #endregion

        #region Public Properties
        public bool State
        {
            get { return this._State; }
            set
            {
                this._State = value;
                FirePropertyChanged();
            }
        }
        #endregion

        #region Public Properties
        public bool NextState { get; set; }
        public int XCo { get; set; }
        public int YCo { get; set; }
        #endregion
    }
}
