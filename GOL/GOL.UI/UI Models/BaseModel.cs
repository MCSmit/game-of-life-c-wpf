﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GOL.UI.UI_Models
{
    public class BaseModel: INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void FirePropertyChanged([CallerMemberName]string property = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion
    }
}
