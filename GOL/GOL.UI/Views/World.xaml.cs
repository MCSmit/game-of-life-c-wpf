﻿using GOL.UI.View_Models;

namespace GOL.UI.Views
{
    /// <summary>
    /// Interaction logic for GOL.xaml
    /// </summary>
    public partial class World
    {
        public World()
        {
            InitializeComponent();
            var model = new WorldViewModel();
            model.Set(this.CellDataGrid);
            this.DataContext = model;
        }
    }
}
