﻿using GOL.UI.UI_Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace GOL.UI.View_Models
{
    public class WorldViewModel: BaseViewModel
    {
        #region Private Fields
        private System.Timers.Timer _EvolutionTimer;
        private DataGrid _CellDataGrid;
        private int _GridWidth = 20;
        private int _GridHeight = 20;
        private ObservableCollection<GridRow> _CellGrid = new ObservableCollection<GridRow>();
        private readonly Tuple<int, int>[] _Pattern1 = new Tuple<int, int>[] {
            new Tuple<int, int>(10, 9),
            new Tuple<int, int>(9, 10),
            new Tuple<int, int>(10, 8),
            new Tuple<int, int>(11, 10),
            new Tuple<int, int>(9, 11),
            new Tuple<int, int>(11, 11),
            new Tuple<int, int>(10, 12)};
        private readonly Tuple<int, int>[] _Pattern2 = new Tuple<int, int>[] {
            new Tuple<int, int>(10, 6),
            new Tuple<int, int>(10, 7),
            new Tuple<int, int>(10, 8),
            new Tuple<int, int>(10, 9),
            new Tuple<int, int>(10, 10),
            new Tuple<int, int>(10, 11),
            new Tuple<int, int>(10, 12),
            new Tuple<int, int>(10, 13),
            new Tuple<int, int>(10, 14),
            new Tuple<int, int>(10, 15)};
        private readonly Tuple<int, int>[] _Pattern3 = new Tuple<int, int>[] {
            new Tuple<int, int>(7, 7),
            new Tuple<int, int>(8, 8),
            new Tuple<int, int>(9, 6),
            new Tuple<int, int>(9, 7),
            new Tuple<int, int>(9, 8)};
        private int _CurrentGeneration = 0;
        private int _CurrentLiveCellCount = 0;
        private int _CurrentDeadCellCount = 0;
        private int _HighestLiveCellCount = 0;
        private bool _IsGameActive;
        private KeyValuePair<int, string> _SelectedPattern = new KeyValuePair<int, string>(1, "Small Explosion");
        private int _TimerInterval = 1000;
        #endregion

        #region Public Properties
        public int GridWidth
        {
            get { return this._GridWidth; }
            set
            {
                if (value > 0)
                    this._GridWidth = value;
                FirePropertyChanged();
            }
        }
        public int GridHeight
        {
            get { return this._GridHeight; }
            set
            {
                if (value > 0)
                    this._GridHeight = value;
                FirePropertyChanged();
            }
        }
        public bool IsGameActive
        { get { return this._IsGameActive; }
        set
            {
                this._IsGameActive = value;
                FirePropertyChanged();
            }
        }
        public Dictionary<int, string> Patterns { get; } = new Dictionary<int, string>()
        {
            { 0, "None" },
            { 1, "Small Explosion" },
            { 2, "10 Row" },
            { 3, "Small Glider" }
        };
        public KeyValuePair<int, string> SelectedPattern
        {
            get { return this._SelectedPattern; }
            set
            {
                this._SelectedPattern = value;
                FirePropertyChanged();
                this.Reset();
            }
        }
        public int CurrentGeneration
        {
            get { return this._CurrentGeneration; }
            set
            {
                this._CurrentGeneration = value;
                FirePropertyChanged();
            }
        }
        public int CurrentLiveCellCount
        {
            get { return this._CurrentLiveCellCount; }
            set
            {
                this._CurrentLiveCellCount = value;
                FirePropertyChanged();
            }
        }
        public int CurrentDeadCellCount
        {
            get { return this._CurrentDeadCellCount; }
            set
            {
                this._CurrentDeadCellCount = value;
                FirePropertyChanged();
            }
        }
        public int HighestLiveCellCount
        {
            get { return this._HighestLiveCellCount; }
            set
            {
                this._HighestLiveCellCount = value;
                FirePropertyChanged();
            }
        }
        public int TimerInterval
        {
            get { return _TimerInterval; }
            set
            {
                this._TimerInterval = value;
                FirePropertyChanged();
                this._EvolutionTimer.Interval = value;
            }
        }
        #endregion

        #region Public Collections
        public ObservableCollection<GridRow> CellGrid
        {
            get { return this._CellGrid; }
            set
            {
                this._CellGrid = value;
                FirePropertyChanged();
            }
        }
        #endregion

        #region Commands
        public ICommand StartCommand { get { return new Commands.StartCommand(this); } }
        public ICommand PauseCommand { get { return new Commands.PauseCommand(this); } }
        public ICommand StepCommand { get { return new Commands.StepCommand(this); } }
        public ICommand StopCommand { get { return new Commands.StopCommand(this); } }
        public ICommand ResetCommand { get { return new Commands.ResetCommand(this); } }
        public ICommand CloseCommand { get { return new Commands.CloseCommand(this); } }
        #endregion

        #region Public Methods
        public void Set(DataGrid dataGrid)
        {
            this._CellDataGrid = dataGrid;
            this.InitialiseWorldCollection();
            this.InitialiseWorldGrid();

            this._EvolutionTimer = new System.Timers.Timer(this.TimerInterval);
            this._EvolutionTimer.Elapsed += new System.Timers.ElapsedEventHandler(EvolutionTimeElapsed);
        }
        public void StartEvolution()
        {
            if (this._EvolutionTimer != null)
            {
                this._EvolutionTimer.Start();
                this.IsGameActive = true;
            }
        }
        public void StopEvolution()
        {
            if (this._EvolutionTimer != null)
                this._EvolutionTimer.Stop();

            this.IsGameActive = false;
        }
        public void StepEvolution()
        {
            if (this._EvolutionTimer != null)
                this._EvolutionTimer.Stop();

            this.Evolve();
            this.UpdateWorldCellState();
        }
        public void Reset()
        {
            if (this._EvolutionTimer != null)
                this._EvolutionTimer.Stop();

            this.IsGameActive = false;
            this.CurrentGeneration = 0;
            this.CurrentDeadCellCount = 0;
            this.CurrentLiveCellCount = 0;
            this.HighestLiveCellCount = 0;
            this.InitialiseWorldCollection();
            this.InitialiseWorldGrid();
        }
        #endregion

        #region Private Methods
        private void InitialiseWorldCollection()
        {
            var items = new List<GridRow>();
            for(int iY = 0; iY < this.GridHeight; iY++)
            {
                var row = new GridRow();
                for (int iX = 0; iX < this.GridWidth; iX++)
                {
                    var cellItem = new CellModel() { XCo = iX, YCo = iY};
                    if (this.SelectedPattern.Key > 0)
                        cellItem.State = this.GetPattern(this.SelectedPattern.Key).Any(o => o.Item1 == iX && o.Item2 == iY);
                    row.Row.Add(cellItem);
                }
                items.Add(row);
            }

            this.CellGrid = new ObservableCollection<GridRow>(items);
        }
        private void InitialiseWorldGrid()
        {
            // Create cell style

            var checkBoxStyle = new Style(typeof(CheckBox));
            checkBoxStyle.Setters.Add(new Setter(CheckBox.WidthProperty, 22.0));
            checkBoxStyle.Setters.Add(new Setter(CheckBox.HeightProperty, 22.0));
            checkBoxStyle.Setters.Add(new Setter(CheckBox.MarginProperty, new Thickness() { Left = -2, Top = -2, Right = -1, Bottom = -1 }));
            checkBoxStyle.Setters.Add(new Setter(CheckBox.HorizontalAlignmentProperty, HorizontalAlignment.Stretch));
            checkBoxStyle.Setters.Add(new Setter(CheckBox.VerticalAlignmentProperty, VerticalAlignment.Stretch));
            checkBoxStyle.Setters.Add(new Setter(CheckBox.BackgroundProperty, Brushes.Black));
            checkBoxStyle.Setters.Add(new Setter(CheckBox.ForegroundProperty, Brushes.Black));
            checkBoxStyle.Setters.Add(new Setter(CheckBox.BorderBrushProperty, Brushes.DarkGray));
            // Set background with data trigger
            var backgroundTrigger = new Trigger();
            backgroundTrigger.Property = CheckBox.IsCheckedProperty;
            backgroundTrigger.Value = true;
            backgroundTrigger.Setters.Add(new Setter(CheckBox.BorderBrushProperty, Brushes.GreenYellow));
            backgroundTrigger.Setters.Add(new Setter(CheckBox.BackgroundProperty, Brushes.GreenYellow));
            backgroundTrigger.Setters.Add(new Setter(CheckBox.ForegroundProperty, Brushes.GreenYellow));
            checkBoxStyle.Triggers.Add(backgroundTrigger);

            //Reset Grid (when called from Reset)
            this._CellDataGrid.Columns.Clear();

            // Set the cell style for the grid
            for (int i = 0; i < this.GridWidth; ++i)
            {
                this._CellDataGrid.Columns.Add(new DataGridCheckBoxColumn()
                {
                    Binding = new Binding($"Row[{i}].State")
                    {
                        Mode = BindingMode.TwoWay,
                        UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                    },
                    IsReadOnly = false,
                    IsThreeState = false,
                    ElementStyle = checkBoxStyle
                });
            }
        }
        private void EvolutionTimeElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this._EvolutionTimer?.Stop();
            this.Evolve();
            this.UpdateWorldCellState();
            this._EvolutionTimer?.Start();
        }
        private void Evolve()
        {
            for (int iY = 0; iY < this.GridHeight; iY++)
            {
                foreach (var cell in this.CellGrid[iY].Row)
                {
                    var neighbours = new List<CellModel>();
                    //Top Row
                    if (cell.YCo > 0)
                    {
                        neighbours.AddRange(this.CellGrid[cell.YCo - 1].Row
                            .Where(o => o.XCo == (cell.XCo - 1)
                            || o.XCo == cell.XCo
                            || o.XCo == (cell.XCo + 1)).ToList());
                    }
                    //Middle Row
                    neighbours.AddRange(this.CellGrid[cell.YCo].Row
                        .Where(o => o.XCo == (cell.XCo - 1)
                        || o.XCo == (cell.XCo + 1)).ToList());

                    //Bottom Row
                    if (cell.YCo < (CellGrid.Count - 1))
                    {
                        neighbours.AddRange(this.CellGrid[cell.YCo + 1].Row
                            .Where(o => o.XCo == (cell.XCo - 1)
                            || o.XCo == cell.XCo
                            || o.XCo == (cell.XCo + 1)).ToList());
                    }

                    //Rules
                    if (cell.State && neighbours.Count(o => o.State) <= 1)
                        cell.NextState = false;
                    else if (cell.State && neighbours.Count(o => o.State) >= 4)
                        cell.NextState = false;
                    else if (!cell.State && neighbours.Count(o => o.State) == 3)
                        cell.NextState = true;
                    else
                        cell.NextState = cell.State;
                }
            }
        }
        private void UpdateWorldCellState()
        {
            var liveCount = 0;
            var deadCount = 0;
            for (int iY = 0; iY < this.GridHeight; iY++)
            {
                foreach (var cell in this.CellGrid[iY].Row)
                {
                    cell.State = cell.NextState;
                    if (cell.State)
                        liveCount++;
                    else
                        deadCount++;
                }
            }
            this.CurrentGeneration++;
            this.CurrentLiveCellCount = liveCount;
            this.HighestLiveCellCount = this.CurrentLiveCellCount > this.HighestLiveCellCount ? this.CurrentLiveCellCount : this.HighestLiveCellCount;
            this.CurrentDeadCellCount = deadCount;
        }
        private Tuple<int, int>[] GetPattern(int key)
        {
            //Small Explosion
            if (key == 1)
                return this._Pattern1;

            //Small Explosion
            if (key == 2)
                return this._Pattern2;

            if (key == 3)
                return this._Pattern3;

            //Default: Small Glider
            return new Tuple<int, int>[0];
        }
        #endregion
    }
}
